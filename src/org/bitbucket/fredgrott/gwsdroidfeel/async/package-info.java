/**
 * 
 */
/**
 * We have differences between Android OS versions and the 
 * differences between requirements of single core android devices 
 * and multi-core devices. Unless its strictly an IO blocking thread 
 * that does not do much but block waiting for an http request 
 * we need than to run concurrent on both single core and 
 * multi_core machines.
 * 
 * In a strictly blocking IO waiting for http request low intensity 
 * async task we need to run serially on both single and multi-core devices.
 * Thus, this set of classes is a port to be able to go both ways on 
 * 2.1 and onward with those assumptions in mind.
 * 
 * The background discussion is at:
 * <pre>http://stackoverflow.com/questions/7211684/asynctask-executeonexecutor-before-api-level-11</pre>
 * 
 * The original code is  a port of android open source and than Yorkw's modifications.
 * Yorkw user profile is here:
 * <pre>http://stackoverflow.com/users/873875/yorkw</pre>
 * 
 * Why not ArrayQue and Deque? You will notice in source that we have 
 * ArrayQue and Deque dependencies removed. Prior to Android 2.2 its Java 5 and 
 * Android 2.2 and up its a Java 6 and its a java version difference dependency.
 * Thus no need to go through and make an attempt to port ArrayQue and Deque.
 * 
 * You will notice that same info in difference use as parsing like for JSOn you 
 * want optimized not to use any ArrayQue  or Deque stuff so that you can 
 * run on anything prior to Android 2.2 fast.
 * 
 * 
 * 
 * @author fredgrott
 *
 */
package org.bitbucket.fredgrott.gwsdroidfeel.async;