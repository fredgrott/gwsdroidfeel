GWSDroidFeel
---

An Android Project Library to make getting consistent 
look and feel on multiple andorid OS versions easier.

# License

# Dependencies

# Project Website

Project webiste has usage directions, the api backport charts and other
useful stuff.

[]()


# Credits

## Code from other projects

See this API chart from my website for this project  to view 
which apis have been backported by what particular project:

[]()



### From Jake Wharten

[ActionBarSherlock]()

[NineOldandroids]()

[HanselAndGretel]()

[ActivityCompat2]()

[NotificationCompat2]()

[SwipeToDismissNOA]()

[ViewPageIndicator]()

### From

[SlidingMenu]()

### From

[SatelliteMenu]()

### From

[RoboGuiceSherlock]()


### From

[AndroidUITableView]()

### From

[AndroidAppMsg]()

### From Square

[Seismic]()

[Otto]()


### From

[Graphview]()

### From

[FlipView]()


### From

[TouchLib]()

### From

[GridLayout]()

### From

[AndroidPullToRefresh]()

### From

[StickyListHeaders]()

[StickyScrollView]()

### From

[AndroidDashboard]()

### From

[SectionAdapter]()

### From

[AndroidFormEditText]()

### From

[AndroidCollapsibleSearch]()

### From

[AndroidSwitchBackport]()

### From

[Segcontrol]()

### Fromm

[Animation Backport]()


### From

[HoloEverywhere]()

### From Tom Barrasso

[GridFragment]()

[ICSProperty]()


### From 

[ViewPager3D]()

[DockableView]()

### From

[HelpViewPager]()



### From

[VewPagerExtensions]()





## Code Contributors

[Fred Grott]()

